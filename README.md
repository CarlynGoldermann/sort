# Welcome
Collected here are several algorithms for sorting the visibility of their speed.

## In the presence of:
* bubble - [Bubble sort](http://en.wikipedia.org/wiki/Bubble_sort) - Simplest but most power algorithms available.
* minElement - [Selection sort](http://en.wikipedia.org/wiki/Selection_sort) - It will select and re-tract the whole selection process and compare and do the sorting. [This website](http://jseo.com/) has the more information. 
* inserts - [Insertion sort](http://en.wikipedia.org/wiki/Insertion_sort) - It will insert elements in different dummy variables and at the end the whole array of elements is sorted
* shell - [Shell sort](http://en.wikipedia.org/wiki/Shellsort)
* heap - [Heap sort](http://en.wikipedia.org/wiki/Heapsort)- Using the concept of heap to sort the array.
* quick - [Quick sort](http://en.wikipedia.org/wiki/Quicksort)
* sort - Default js array sort [].sort()

## How to use
1. Open  index.html in your browser
2. Open debug console 
3. Run:
```
#!javascript
Sort.test()

```

### Sort.test(int arrLength, int numberLength, int numberTimes)
* **arrLength** - array length - default 1000000
* **numberLength** - number length - default 7
* **numberTimes** - number times - default 3

### Sort.view(obj params)
Default displaing in console.log().  
Params gets from Sort.test and has: **arr**, **name**, **time** - middle, **times** - array with time iteration.

### Object Sort(int arrLength, int numberLength)
* **arr** - internal array
* **bubble()**
* **minElement()**
* **inserts()**
* **shell()**
* **heap()**
* **quick()**
* **sort()**

All methods returned **params**