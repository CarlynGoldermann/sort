var Sort = (function(){

	
	function Sort (arrLength, numberLength){
		this.arr = _fillArray(arrLength, numberLength);		
	}

	Sort.test = function (arrLength, numberLength, numberTimes){	
		var i, j, sort, time
		, ret = {}
		, middle = {}
		, participant = []
		, results = {}
		, times = [];

		arrLength = arrLength || 1000000	
		numberLength = numberLength || 7	
		numberTimes = numberTimes || 3

		sort = new Sort(arrLength, numberLength);	

		participant = [			
//			sort.bubble,
//			sort.minElement,		
//			sort.inserts,
			sort.shell,
			sort.heap,
			sort.quick,
			sort.sort,
		]

		console.log('Start:', participant.length, 'participant, arrLength =', arrLength,', numberLength =', numberLength, ', numberTimes =', numberTimes,'.\nPlease wait...');

		for(i=0; i<participant.length; i++){			
			times = []
			time = 0
			for(j=1; j<=numberTimes; j++){
				results = participant[i].call(sort);
				time += results.time;
				times.push(results.time)
			};
						
			
			ret[results.name] = {
				name : results.name,
				arr: results.arr,
				time: time / numberTimes ^0,
				times: times
			};

			view(ret[results.name]);
		};
		
		console.log(' ...done.');
		
		ret.sort = sort
		return ret;
	}

	Sort.view = view = function (params){
		console.log(params.name +': '+ params.time +' ms') 

	}


	Sort.prototype.bubble = function () {
		var i, j, passedTime
		, arr = _clone(this.arr)
		, timeStop = _timeStart();		
		
		for (i=0; i<arr.length; i++){			
			for (j=i+1; j<arr.length-1; j++){								

				if (arr[j]>arr[j+1]){
					_permutation(arr, j, j+1)
				}
			}
		}
		
		passedTime = timeStop();

		return {	
			name: 'bubble',
			arr: arr,
			time: passedTime
		};
	}

	
	Sort.prototype.minElement = function (){
		var i, j, passedTime, pos, tmp
		, arr = _clone(this.arr)
		, timeStop = _timeStart();		
				
		for (i=0; i<arr.length; i++){			
			pos = i;
			tmp = arr[i];
			
			for (j = i + 1; j<arr.length; j++){
				
				if(arr[j] < tmp){					
					pos = j;
					tmp = arr[j];
				}

			}	

			_permutation(arr, i, pos);

		}

		passedTime = timeStop();

		return {	
			name: 'minElement',		
			arr: arr,
			time: passedTime
		};
	}


	Sort.prototype.inserts = function (){
		var i, j, tmp, passedTime
		, arr = _clone(this.arr)
		, timeStop = _timeStart();
				
		for (i=0; i<arr.length; i++){					
			tmp = arr[i];
			
			for (j = i - 1; j>=0 && arr[j] > tmp; j--){
				arr[j+1] = arr[j];										
			}	
				
			arr[j+1] = tmp;						

		}

		passedTime = timeStop();

		return {	
			name: 'inserts',		
			arr: arr,
			time: passedTime
		};
	}


	Sort.prototype.shell = function (){
		var i, j, tmp, s, inc, passedTime
		, seq=[]
		, arr = _clone(this.arr)
		, size = arr.length
		, timeStop = _timeStart();		
	
		function increment(inc, size) {
		  var p1, p2, p3, s;

		  p1 = p2 = p3 = 1;
		  s = -1;
		  do {
		    if (++s % 2) {
		      inc[s] = 8*p1 - 6*p2 + 1;
		    } else {
		      inc[s] = 9*p1 - 9*p3 + 1;
		      p2 *= 2;
		      p3 *= 2;
		    }
			p1 *= 2;
		  } while(3*inc[s] < size);  

		  return s > 0 ? --s : 0;
		}
	
		s = increment(seq, size);
		while (s >= 0) {				
			inc = seq[s--];			
			for (i = inc; i < size; i++) {			  
			  tmp = arr[i];
			  for (j = i-inc; (j >= 0) && (arr[j] > tmp); j -= inc){
			    arr[j+inc] = arr[j];		
			  }
			  arr[j+inc] = tmp;			  
			}
		}
		

	    passedTime = timeStop();

		return {	
			name: 'shell',		
			arr: arr,
			time: passedTime
		};
	}


	Sort.prototype.heap = function (){		
		var i, j, passedTime
		, arr = _clone(this.arr)		
		, size = arr.length
		, timeStop = _timeStart();
			
		function downHeap(arr, i, size){
		    var child
		    , new_elem = arr[i];		    

		    while(i <= size/2){       		        
		        child = 2*i;		        
		        
		        if( child < size && arr[child] < arr[child+1] )
		            child++;

		        if( new_elem >= arr[child] ) 
		            break; 
		        
		        arr[i] = arr[child];
		        i = child;		        
		    }
		    arr[i] = new_elem;		    		   
		}
		 			  	
	    for(i = size / 2 - 1; i >= 0; --i)	    	
	        downHeap(arr, i, size-1);	  		        
	        	    	
	    for(i=size-1; i > 0; --i){
	        _permutation(arr, i, 0);
	        downHeap(arr, 0, i-1); 
	    }
		
	    passedTime = timeStop();

		return {
			name: 'heap',			
			arr: arr,
			time: passedTime
		};
	} 

	
	Sort.prototype.quick = function (){		
		var passedTime
		, arr = _clone(this.arr)
		, size = arr.length
		, timeStop = _timeStart();
		
		function sort(left, right){
			var p = arr[left+right>>1]
			, i = left
			, j = right;

			do {
				while ( arr[i] < p ) i++;
				while ( arr[j] > p ) j--;

				if (i <= j) {
				  _permutation(arr, i, j)
				  i++; j--;
				}
			} while ( i<=j );			
			
			if ( left < j ) sort(left, j);
			if ( right > i ) sort(i, right);	
		}
		
		sort(0, size-1)
		
		passedTime = timeStop();

		return {	
			name: 'quick',
			arr: arr,
			time: passedTime
		};
	}


	Sort.prototype.sort = function (){		
		var passedTime
		, arr = _clone(this.arr)
		, timeStop = _timeStart();		
		
		arr.sort();		
		
		passedTime = timeStop();

		return {
			name: 'sort',
			arr: arr,
			time: passedTime
		};
	}


	function _timeStart () {
		var start, stop;		
		
		start = Date.now();
		
		return _timeStop = function (){			
			return Date.now() - start;
		}		

	}


	function _clone(arr){
		var newArr = [];
		
		for (var i=0; i<arr.length; i++){			
			newArr[i] = arr[i];
		}
		
		return newArr;
	}


	function _permutation (arr, x, y){
		var temp = arr[x];

		arr[x] = arr[y];
		arr[y] = temp;					
	}


	function _fillArray (arrLength, numberLength){
		var num = 0
		, retArr = [];
		
		for (var i=0; i<arrLength; i++){
			num = _getRandomNumber(numberLength);
			retArr.push(num);
		}
		
		return retArr;
	}


	function _getRandomNumber (length){
		var str, num;
		
		if (length > 16) length = 2;
		
		str = Math.random()+'';
		str = str.substr(2, length);		
		num = str >>0;
		
		return num;
	}

	return Sort;
})()




